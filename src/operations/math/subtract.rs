use crate::network::node::Node;
use core::ops;
use alloc::boxed::Box;
use core::any::Any;
use crate::helper::iter::iter_n_inputs;

/// Operation to subtract two or more inputs together
pub struct Subtract {}

impl Subtract {

    /// Generate a new add operation
    ///
    /// ## Example
    /// ```rust
    /// use sac_base::operations::math::subtract::Subtract;
    /// let mut sub = Subtract::new();
    /// let mut vec = Vec::new();
    /// vec.push(&[1.0, 3.0, 4.0][..]);
    /// vec.push(&[2.0, 2.0][..]);
    /// vec.push(&[3.0][..]);
    /// // Subtracts up the three given inputs.
    /// sub.process((vec, 3));
    /// assert_eq!(sub.poll(), &[-4.0, 1.0, 4.0][..]);
    /// ```
    pub fn new<T>() -> Node<T>
        where T: Copy + ops::Sub<Output=T> + Default
    {
        let storage = Box::new(0) as Box<dyn Any>;
        Node::new(storage,  |data_input, _input, output| {
            iter_n_inputs(data_input, output, |left: T, right: T| {
                return left - right;
            });
        })
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use alloc::vec::Vec;

    #[test]
    fn test_add() {

        let mut inputs = Vec::new();
        inputs.push(&[1.0][..]);
        inputs.push(&[2.0][..]);

        let mut add = Subtract::new();
        add.process((inputs, 1));

        assert_eq!(add.poll(), &[-1.0][..]);
    }

    #[test]
    fn test_multiple() {

        let mut inputs = Vec::new();
        inputs.push(&[1.0][..]);
        inputs.push(&[2.0][..]);
        inputs.push(&[3.0][..]);

        let mut add = Subtract::new();
        add.process((inputs, 1));

        assert_eq!(add.poll(), &[-4.0][..]);
    }

    #[test]
    fn test_long_slice() {

        let mut inputs = Vec::new();
        inputs.push(&[1.0, 2.0][..]);
        inputs.push(&[2.0, 1.0][..]);
        inputs.push(&[3.0, 5.0, 1.0][..]);

        let mut add = Subtract::new();
        add.process((inputs, 3));

        assert_eq!(add.poll(), &[-4.0, -4.0, -1.0][..]);
    }

    #[test]
    fn test_multiple_runs() {

        let mut inputs = Vec::new();
        inputs.push(&[1.0][..]);
        inputs.push(&[2.0][..]);

        let mut add = Subtract::new();
        add.process((inputs, 1));


        let mut inputs = Vec::new();
        inputs.push(&[5.0][..]);
        inputs.push(&[6.0][..]);
        add.process((inputs, 1));

        assert_eq!(add.poll(), &[-1.0][..]);
    }
}

