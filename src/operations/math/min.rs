use crate::network::node::Node;
use alloc::boxed::Box;
use core::any::Any;
use alloc::collections::VecDeque;


/// Calculates the min value over n samples given
pub struct Min<T> {
    samples: VecDeque<T>,
}

impl<T> Min<T>
    where T: Copy + Default + 'static + PartialOrd
{
    /// Generate a new min operation
    ///
    /// ## Example
    /// ```rust
    /// use sac_base::operations::math::min::Min;
    /// use sac_base::network::node::Node;
    ///
    /// let mut min = Min::new(3) as Node<f32>;
    /// let mut inputs = Vec::new();
    /// inputs.push(&[1.0, 5.0, 6.0, 2.0, 11.0, 12.0, 3.0][..]);
    /// min.process((inputs, 7));
    /// assert_eq!(min.poll(), &[3.0][..]);
    /// let mut inputs = Vec::new();
    /// inputs.push(&[1.0, 5.0][..]);
    /// min.process((inputs, 2));
    /// assert_eq!(min.poll(), &[1.0][..]);
    /// ```
    pub fn new(over_n: usize) -> Node<T> {
        let mut min = Min {
            samples: VecDeque::with_capacity(over_n),
        };

        for _i in 0..over_n {
            min.samples.push_back(T::default());
        }

        let storage = Box::new(min) as Box<dyn Any>;
        Node::new(storage,  |data_input, data_container, output| {
            let min: &mut Min<T> = data_container.downcast_mut::<Min<T>>().unwrap();
            let (inputs, max_len) = data_input;

            inputs.into_iter().take(1).into_iter().for_each(|data| {
                data.into_iter().take(max_len).into_iter().for_each(|v| {
                    min.samples.pop_front();
                    min.samples.push_back(*v);

                    if let Some(start) = min.samples.get(0) {
                        let mut current_min = *start;

                        for i in 1..min.samples.len() {

                            if let Some(v) = min.samples.get(i) {
                                if *v < current_min {
                                    current_min = *v;
                                }
                            }
                        }

                        output.clear();
                        output.insert(0, current_min);
                    }
                })
            })
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use alloc::vec::Vec;

    #[test]
    fn test_min() {
        let mut min = Min::new(5) as Node<f32>;

        let mut inputs = Vec::new();
        inputs.push(&[1.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[5.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[6.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[2.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[11.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[12.0][..]);
        min.process((inputs, 1));

        let mut inputs = Vec::new();
        inputs.push(&[3.0][..]);
        min.process((inputs, 1));

        assert_eq!(min.poll(), &[2.0][..]);
    }

    #[test]
    fn test_min_slice() {
        let mut min = Min::new(3) as Node<f32>;

        let mut inputs = Vec::new();
        inputs.push(&[1.0, 5.0, 6.0, 2.0, 11.0, 12.0, 3.0][..]);
        min.process((inputs, 7));
        assert_eq!(min.poll(), &[3.0][..]);

        let mut inputs = Vec::new();
        inputs.push(&[1.0, 5.0][..]);
        min.process((inputs, 2));

        assert_eq!(min.poll(), &[1.0][..]);
    }

    #[test]
    fn test_min_integer() {
        let mut min = Min::new(3) as Node<i16>;

        let mut inputs = Vec::new();
        inputs.push(&[1, 5, -6, 2, -11, 12, 3][..]);
        min.process((inputs, 7));
        assert_eq!(min.poll(), &[-11][..]);

        let mut inputs = Vec::new();
        inputs.push(&[1, -5][..]);
        min.process((inputs, 2));

        assert_eq!(min.poll(), &[-5][..]);
    }
}