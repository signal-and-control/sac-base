use crate::network::node::Node;
use alloc::boxed::Box;
use core::any::Any;
use core::ops;

/// Simple operation which just inverts the last set input.
pub struct Invert {}

impl Invert {

    /// Generate a new inverter
    ///
    /// ## Example
    /// ```rust
    /// use sac_base::operations::miscellaneous::invert::Invert;
    /// let mut invert = Invert::new();
    /// let mut vec = Vec::new();
    /// vec.push(&[1.0, 22.0, -13.0][..]);
    /// invert.process((vec, 3));
    /// assert_eq!(invert.poll(), &[-1.0, -22.0, 13.0][..]);
    /// let mut vec = Vec::new();
    /// vec.push(&[55.0, -10.0][..]);
    /// invert.process((vec, 2));
    /// assert_eq!(invert.poll(), &[-55.0, 10.0][..]);
    /// ```
    pub fn new<T>() -> Node<T>
        where T: Copy + Default + ops::Sub<Output=T>
    {
        let storage = Box::new(0) as Box<dyn Any>;
        Node::new(storage, |data_input, _store, output| {
            let (inputs, max_len) = data_input;
            output.clear();
            inputs.into_iter().take(1).into_iter().for_each(|data| {
                data.into_iter().take(max_len).into_iter().for_each(|v| {
                    output.push(*v - *v - *v);
                })
            });
        })
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use alloc::vec::Vec;

    #[test]
    fn test_invert() {
        let mut invert = Invert::new();
        let mut vec = Vec::new();
        vec.push(&[1.0][..]);
        invert.process((vec, 1));
        assert_eq!(invert.poll(), &[-1.0][..]);
        let mut vec = Vec::new();
        vec.push(&[55.0][..]);
        invert.process((vec, 1));
        assert_eq!(invert.poll(), &[-55.0][..]);
        let mut vec = Vec::new();
        vec.push(&[66.0][..]);
        invert.process((vec, 1));
        assert_eq!(invert.poll(), &[-66.0][..]);
    }

    #[test]
    fn test_slice() {
        let mut invert = Invert::new();
        let mut vec = Vec::new();
        vec.push(&[1.0, 22.0, -13.0][..]);
        invert.process((vec, 3));
        assert_eq!(invert.poll(), &[-1.0, -22.0, 13.0][..]);
        let mut vec = Vec::new();
        vec.push(&[55.0, -10.0][..]);
        invert.process((vec, 2));
        assert_eq!(invert.poll(), &[-55.0, 10.0][..]);
    }
}

