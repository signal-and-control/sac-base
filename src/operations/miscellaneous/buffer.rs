use crate::network::node::Node;
use alloc::boxed::Box;
use core::any::Any;

/// Simple operation which just buffers the last set input.
/// This is as example used as input.
pub struct Buffer {}

impl Buffer {

    /// Generate a new buffer
    ///
    /// ## Example
    /// ```rust
    /// use sac_base::operations::miscellaneous::buffer::Buffer;
    /// let mut buffer = Buffer::new();
    /// buffer.feed(&[1.0][..]);
    /// assert_eq!(buffer.poll(), &[1.0][..]);
    /// buffer.feed(&[5.0][..]);
    /// assert_eq!(buffer.poll(), &[5.0][..]);
    /// buffer.feed(&[10.0][..]);
    /// assert_eq!(buffer.poll(), &[10.0][..]);
    /// ```
    pub fn new<T>() -> Node<T>
        where T: Copy + Default
    {
        let storage = Box::new(0) as Box<dyn Any>;
        Node::new(storage, |_data_input, _input, _output| {})
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_buffer() {
        let mut buffer = Buffer::new();
        buffer.feed(&[1.0][..]);
        assert_eq!(buffer.poll(), &[1.0][..]);
        buffer.feed(&[5.0][..]);
        assert_eq!(buffer.poll(), &[5.0][..]);
        buffer.feed(&[10.0][..]);
        assert_eq!(buffer.poll(), &[10.0][..]);
    }
}

