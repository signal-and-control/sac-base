use alloc::vec::Vec;

#[inline(always)]
pub fn iter_n_inputs<T, F>(data_input: (Vec<&[T]>, usize), output: &mut Vec<T>, clk: F)
    where   T: Default + Copy,
            F: Fn(T, T) -> T
{
    let (inputs, max_len) = data_input;
    output.clear();
    for i in 0..max_len {
        let mut res = match inputs.get(0) {
            Some(inner) => {
                match inner.get(i) {
                    Some(v) => *v,
                    None => T::default(),
                }
            },
            None => T::default()
        };

        for j in 1..inputs.len() {
            let val = match inputs.get(j) {
                Some(inner) => {
                    match inner.get(i) {
                        Some(v) => *v,
                        None => T::default(),
                    }
                },
                None => T::default()
            };
            res = clk(res, val);
        }
        output.push(res);
    }
}

pub fn iter_one_input_to_one<T, F>(data_input: (Vec<&[T]>, usize), output: &mut Vec<T>, clk: F)
    where   T: Default + Copy,
            F: Fn(T) -> T
{
    let (inputs, max_len) = data_input;
    inputs.into_iter().take(1).into_iter().for_each(|data| {
        data.into_iter().take(max_len).into_iter().for_each(|v| {
            let res = clk(*v);
            output.insert(0, res);
        })
    })
}